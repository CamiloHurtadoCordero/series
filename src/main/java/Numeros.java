/**
 * Created by Camilo on 30/04/2018.
 */
public class Numeros {
    public void seriesPares(int n){
        int V= 2;
        int [] a= new int[n];
        for(int i=0; i<n; i++){
            a[i]=V;
            V=V+2;
            System.out.print(a[i]+" ");
        }


    }
    public void serieFibonacci(int n){
        int [] a= new int[n];
        a[0]=0;
        a[1]=1;
        if(n==1){
            System.out.print(a[0]+" ");
        } else if(n==2){
            System.out.print(a[0]+" "+a[1]);
        }
        for(int i=2; i<n; i++){
            a[i]=a[i-2]+a[i-1];
            System.out.print(a[i]+" ");
        }
    }

    public void seriesPrimos(int e){
        int [] a= new int[e];
        int i=0;
        int c = 1;
        int p = 2;
        int d = 2;
        while (c <= e) {
            if (p % d == 0){
                if (p== d){
                    a[i]=p;
                    System.out.print( a[i]+ " ");
                    i++;
                    c++;
                }
                d = 2;
                p++;
            }
            else
                d++;
        }


    }

}
